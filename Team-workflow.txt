jamek066@W7-PC0HYU1B MINGW64 ~/Desktop
$ git clone git@gitlab.disney.com:GitLab-Training/Git-101.git
Cloning into 'Git-101'...
*** WARNING ***

THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
Users (authorized or unauthorized) have no explicit or implicit
expectation of privacy. THERE IS NO RIGHT OF PRIVACY IN THIS SYSTEM.
System personnel may disclose any potential evidence of crime found
on computer systems for any reason.  USE OF THIS SYSTEM BY ANY USER,
AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO THIS MONITORING,
INTERCEPTION, RECORDING, READING, COPYING, or CAPTURING and DISCLOSURE.

remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git checkout -b Lilo
Switched to a new branch 'Lilo'

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (Lilo)
$ git add .

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (Lilo)
$ git status
On branch Lilo
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   Personal-workflow.txt
        new file:   Team-workflow.txt


jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (Lilo)
$ git commit -m "adding workflow examples"
[Lilo e2131c0] adding workflow examples
 2 files changed, 86 insertions(+)
 create mode 100644 Personal-workflow.txt
 create mode 100644 Team-workflow.txt

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (Lilo)
$ git push origin Lilo
*** WARNING ***

THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
Users (authorized or unauthorized) have no explicit or implicit
expectation of privacy. THERE IS NO RIGHT OF PRIVACY IN THIS SYSTEM.
System personnel may disclose any potential evidence of crime found
on computer systems for any reason.  USE OF THIS SYSTEM BY ANY USER,
AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO THIS MONITORING,
INTERCEPTION, RECORDING, READING, COPYING, or CAPTURING and DISCLOSURE.

Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 1.32 KiB | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for Lilo, visit:
remote:   https://gitlab.disney.com/GitLab-Training/Git-101/merge_requests/new?merge_request%5Bsource_branch%5D=Lilo
remote:
To gitlab.disney.com:GitLab-Training/Git-101.git
 * [new branch]      Lilo -> Lilo

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (Lilo)
$ git checkout master
Switched to branch 'master'
Your branch is up-to-date with 'origin/master'.

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git branch -l
  Lilo
* master

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git pull
*** WARNING ***

THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
Users (authorized or unauthorized) have no explicit or implicit
expectation of privacy. THERE IS NO RIGHT OF PRIVACY IN THIS SYSTEM.
System personnel may disclose any potential evidence of crime found
on computer systems for any reason.  USE OF THIS SYSTEM BY ANY USER,
AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO THIS MONITORING,
INTERCEPTION, RECORDING, READING, COPYING, or CAPTURING and DISCLOSURE.

remote: Counting objects: 1, done.
remote: Total 1 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (1/1), done.
From gitlab.disney.com:GitLab-Training/Git-101
   cef2cbb..f90792d  master     -> origin/master
Updating cef2cbb..f90792d
Fast-forward
 Personal-workflow.txt | 66 +++++++++++++++++++++++++++++++++++++++++++++++++++
 Team-workflow.txt     | 20 ++++++++++++++++
 2 files changed, 86 insertions(+)
 create mode 100644 Personal-workflow.txt
 create mode 100644 Team-workflow.txt

