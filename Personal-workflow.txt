jamek066@W7-PC0HYU1B MINGW64 ~/Desktop
$ git clone git@gitlab.disney.com:GitLab-Training/Git-101.git
Cloning into 'Git-101'...
*** WARNING ***

THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
Users (authorized or unauthorized) have no explicit or implicit
expectation of privacy. THERE IS NO RIGHT OF PRIVACY IN THIS SYSTEM.
System personnel may disclose any potential evidence of crime found
on computer systems for any reason.  USE OF THIS SYSTEM BY ANY USER,
AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO THIS MONITORING,
INTERCEPTION, RECORDING, READING, COPYING, or CAPTURING and DISCLOSURE.

remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (3/3), done.

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop
$ cd Git-101/

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git remote -v
origin  git@gitlab.disney.com:GitLab-Training/Git-101.git (fetch)
origin  git@gitlab.disney.com:GitLab-Training/Git-101.git (push)

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git add .

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   Git101.txt
        new file:   Test.txt


jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git commit -m "This is the first commit - woo"
[master cef2cbb] This is the first commit - woo
 2 files changed, 2 insertions(+)
 create mode 100644 Git101.txt
 create mode 100644 Test.txt

jamek066@W7-PC0HYU1B MINGW64 ~/Desktop/Git-101 (master)
$ git push -u origin master
*** WARNING ***

THIS IS A PRIVATE COMPUTER SYSTEM. It is for authorized use only.
Users (authorized or unauthorized) have no explicit or implicit
expectation of privacy. THERE IS NO RIGHT OF PRIVACY IN THIS SYSTEM.
System personnel may disclose any potential evidence of crime found
on computer systems for any reason.  USE OF THIS SYSTEM BY ANY USER,
AUTHORIZED OR UNAUTHORIZED, CONSTITUTES CONSENT TO THIS MONITORING,
INTERCEPTION, RECORDING, READING, COPYING, or CAPTURING and DISCLOSURE.

Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (4/4), 355 bytes | 0 bytes/s, done.
Total 4 (delta 0), reused 0 (delta 0)
To gitlab.disney.com:GitLab-Training/Git-101.git
   af418a9..cef2cbb  master -> master
Branch master set up to track remote branch master from origin.
