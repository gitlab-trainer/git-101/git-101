Tutorials
- https://rogerdudler.github.io/git-guide/
- https://git-scm.com/book/en/v2/Getting-Started-Git-Basics\
- https://www.youtube.com/watch?v=4SD6rWt9wUQ
- https://www.youtube.com/watch?v=Y9XZQO1n_7c
- https://www.atlassian.com/git/tutorials

Cheat Sheets
- https://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf
- https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
- https://about.gitlab.com/images/press/git-cheat-sheet.pdf